
//ejercicio 1

function miFuncionDeComparar(numero1, numero2) {
    // Recuerda completar los condicionales 'if'

    if (numero1 === 5) {
        console.log("numero1 es estrictamente igual a 5");
    }

    if (numero1<numero2) {
        console.log("numero1 no es mayor que numero2");
    }
    if (numero2>=0) {
        console.log("numero2 es positivo");
    }
    if (numero1<0 || numero1!=0) {
        console.log("numero1 es negativo o distinto de cero");
    }
}

var res = miFuncionDeComparar(5, 2);

//dados estos valores, también imprime: "numero1 es negativo o distinto a 0, o debería poner otro if a propósito para que no se imprima?"


